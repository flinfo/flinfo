<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2011 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('FlinfoHooks.php');
require_once ('FlinfoData.php');
require_once ('lib/Curly.php');
require_once ('lib/FormatJson.php');

/**
 * Interface for calling exiftool to get EXIF information about a URL. Why do we use this instead of
 * exif_read_data()?
 *
 * 1. exif_read_data() cannot read from URLs (according to the documentation at php.net). That would
 *    mean we'd have to fully download a file, which may be huge, just to get the EXIF, which is a
 *    few 10kB.
 * 2. exif_read_data() can handle only a limited number of MIME types (jpg and tiff, I think). exiftool
 *    can handle *much* more.
 * 3. EXIF readers may be buggy. If exiftool should crash, it doesn't take us down, too, since it runs
 *    in a child process.
 */
class FlinfoExtExiftool {

	public static $instance = null;

	// Configure this for your environment! Not consts because they could be dynamically configured.

	public static $use_exiftool = false; // Set to true once you have set the pathes below correctly.
	public static $exiftool_dir = './exiftool'; // Directory of the exiftool executable, if we're to CD there. Relative to the path of this file.
	public static $exiftool_name = './exiftool'; // Name (and path) of the exiftool executable

	/*
	 * Here's a native Windows config
	 *
	 * $exiftool_dir = null;
	 * $exiftool_name = '"C:\Bin\EXIFTool\exiftool.exe"';
	 */

	const EXIFTOOL_ARGS = '-fast -G0 -n -q -q -j -'; // JSON output to avoid problems with control characters and binary data

	public static $isWindows = false; // Initialized below

	/**
	 * Parse the result of calling exiftool into a friendly associative array.
	 *
	 * @param array $output Array of lines output by exiftool
	 * @return array
	 */
	private function parseResult ($output) {
		$json = FormatJson::decode ($output, true);
		if (!is_array ($json) || count($json) === 0) return null;
		$json = $json[0];
		$exif = array ();
		foreach ($json as $tagname => $value) {
			// $tagname is TAGSPACE:tag
			$tagname = explode (':', $tagname, 2);
			if (count($tagname) !== 2) continue; // No tagspace or no tagname
			if (!is_array ($value)) {
				$value = trim ("" . $value);
				$value = str_replace ("\r\n", "\n", $value);
				$value = str_replace (array ("\r", '\\'), array ('\\n', '\\\\'), $value);
				$value = preg_replace ('/\s+/', ' ', $value);
				$value = str_replace (array ('\\\\', '\\n'), array ('\\', "\n"), $value);
			}
			$exif[] = array ('tag' => $tagname[1], 'tagspace' => $tagname[0], 'raw' => $value);
		}
		return $exif;
	}

	/**
	 * Download as much of $url needed and pass it into exiftool.
	 *
	 * This turned out to be more complicated to do properly and portably than expected.
	 * simply exec ("wget -qO - $url | exiftool ..."), with $url escapeshellarg'ed of course,
	 * works fine on Unix, but fails on Windows. First, on Windows, the command is run in
	 * cmd.exe or command.com, which doesn't have binary pipes. \n vs. \r\n conversions of
	 * course corrupt the input to exiftool... Second, escapeshellarg on Windows doesn't quite
	 * do what it should. In particular, it replaces % by a blank. Unfortunately, urls may quite
	 * legitimately contain percent signs. (It also has lots of other problems...)
	 *
	 * So I tried seting up "wget -qO - -i -" and exiftool through proc_open, passing wget's stdout
	 * pipe in the descriptor for exiftool as stdin and making sure I set the 'binary_pipes'
	 * 'bypass_shell'. Pipe the two stderr streams to /dev/null (NUL on Windows). Write the uri
	 * into wget's stdin, and stream_get_contents from exiftool's stdout. Works well in Unix,
	 * fails stupidly on Windows; somehow the pipe between the two got converted to text mode,
	 * despite my opening it binary. (Mode 'wb' didn't work at all; Php 5.2.3 doesn't even open
	 * the pipe then.)
	 *
	 * As a final resort I came up with the following solution, which eliminates wget altogether.
	 * We read from $url ourselves using cURL, piping the input directly into a proc_open'ed exiftool
	 * via the CURLOPT_FILE mechanism. Has the advantage that we don't have to fork an extra child
	 * for wget and we also don't need to worry about shell escaping the $url. (Which may be messy
	 * especially on Windows.)
	 *
	 * @param string $url
	 * @return array  a friendly associative array
	 */
	public function getExifFromUrl ($url) {
		if (!self::$use_exiftool) return null;
		$resultFile = tmpfile (); // tmpfiles are removed automatically when fclosed or when this process exits
		if (!$resultFile) return null;
		$descriptor = array(
		   /* stdin  */  0 => array('pipe', 'r') // Child reads; binary
		   /* stdout */ ,1 => $resultFile        // Child writes
		   /* stderr */ ,2 => array('file', self::$isWindows ? 'NUL' : '/dev/null', 'w') // Child writes; ignore
		);
		// Note: we can't execute curl_exec *and* read from the child's stdout in the same process; that'd block at the
		// very latest once the stdout buffer was full. And we can't fork on an Apache web server to run the curl_exec in
		// a child while reading from the stdout pipe in the parent. Hence we direct stdout to a temporary file and read
		// the output from there once curl_exec has returned.
		//   The file needs not be opened in binary mode; exiftool generates ASCII output (in fact, JSON). The binary data
		// from the cURL request goes through the stdin pipe to exiftool.
		$cwd = null;
		if (self::$exiftool_dir) $cwd = dirname(__FILE__) . '/' . self::$exiftool_dir;
		$options = array (
			 'binary_pipes' => true // Otherwise we may on Windows get \n vs. \r\n replacement issues in the downloaded file
			,'bypass_shell' => true // Windows' cmd.exe has no binary pipes and generally mucks around with too many things
		);
		// 'bypass_shell' is ignored on non-Windows platforms.
		$exiftool = proc_open(self::$exiftool_name . ' ' . self::EXIFTOOL_ARGS, $descriptor, $pipes, $cwd, null, $options);
		if (!is_resource($exiftool)) {
			fclose ($resultFile); // Also removes it
			return null;
		}
		$pipe = $pipes[0];
		stream_set_blocking($pipe, 0);
		$request = Curly::getRequest ($url);
		curl_setopt ($request, CURLOPT_FILE, $pipe); // Write to child's stdin
		// Start downloading the $url. Pipe directly into exiftool; the -fast option makes exiftool quit as soon as it has
		// its basic EXIF data. This breaks that pipe, which makes curl_exec fail upon the next write, thus aborting the
		// download once we've got the EXIF. So for instance for the 970kB image at
		// http://farm4.static.flickr.com/3171/5711154852_1d6efa035f_o_d.jpg
		// we'll download only the first 30-60kB and then stop.
		curl_exec ($request);
		curl_close ($request);
		// Close the pipe and then the child process. No need to close the files.
		fclose($pipe);
		$return_value = proc_close ($exiftool);
		// Get the results and close the temporary file, which is thereby removed
		fseek ($resultFile, 0);
		$data = stream_get_contents($resultFile);
		fclose ($resultFile);
		if (!$data) return null;
		return $this->parseResult ($data);
	}

	public function exiftoolHook ($url, &$exif) {
		$exif = $this->getExifFromUrl ($url);
		return true;
	}
}

FlinfoExtExiftool::$isWindows = strpos (strtolower (php_uname ('s')), 'windows') !== false;
FlinfoExtExiftool::$instance = new FlinfoExtExiftool ();
if (is_file (dirname (__FILE__) . '/' . FlinfoExtExiftool::$exiftool_dir . '/' . FlinfoExtExiftool::$exiftool_name)) {
	FlinfoExtExiftool::$use_exiftool = true;
}
FlinfoHooks::register('flinfoExiftool', array (FlinfoExtExiftool::$instance, 'exiftoolHook'));
