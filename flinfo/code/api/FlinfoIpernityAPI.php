<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('lib/Curly.php');

/**
 * Basic ipernity API. Requires CURL.
 * 
 * Only implemented function is what's interesting for flinfo: getting
 * image information including tags and geolocation.
 */
class FlinfoIpernityAPI {
	
	private $apiKey = '';
	private $secret = '';
	
	public function __construct ($apiKey, $secret) {
		$this->apiKey = $apiKey;
		$this->secret = $secret;
	}
	
	/**
	 * Execute an ipernity request for the given function.
	 * @param String $functionName Name of the ipernity function, e.g. "doc.get"
	 * @param array $params Array containing the parameters.
	 * @return unknown_type (String or array)
	 *   If a string is returned, it's an error message. Otherwise, returns the server response as an array.
	 */
	private function request ($functionName, $params) {
		$params['api_key'] = $this->apiKey;
		if ($this->secret) {
			$params['api_sig'] = $this->signParameters ($functionName, $params);
		}
		$url = 'http://api.ipernity.com/api/' . $functionName . '/php';
		
		$req = Curly::postRequest ($url, $params);
		$data = Curly::singleRequest ($req, $errorMsg);
    	if ($errorMsg !== null) {
    		return $errorMsg;
    	}
	    $result = unserialize($data);
	    if (is_array ($result) && isset ($result['api']) && isset ($result['api']['status'])) {
	    	if ($result['api']['status'] != 'ok') {
	    		return $result['api']['message'];
	    	}
	    	return $result;
	    }
		return 'Unknown format of server response.';
	}
	
	/**
	 * Compute the parameter signature.
	 * @param String $functionName Name of the ipernity function to be called.
	 * @param array $params Parameters for the call
	 * @return String MD5-hash
	 */
	private function signParameters ($functionName, $params) {
		ksort ($params);
		$signature = '';
		foreach ($params as $k => $v) {
			$signature .= $k . $v;
		}
		$signature .= $functionName . $this->secret;
		return md5($signature);
	}
	
	/**
	 * Get image inormation from ipernity, including tags and geolocation.
	 * 
	 * @param $imageId ID of the image
	 * @return unknown_type (String or array)
	 *   If a string is returned, it's an error message. Otherwise, returns the server response as an array.
	 */
	public function getInfo ($imageId, $extra = null) {
		$params = array ('doc_id' => $imageId);
		if ($extra) $params['extra'] = $extra;
		return $this->request ('doc.get', $params);
	}

}