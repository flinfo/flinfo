<?php
/**
 * Curly. Simple cURL wrapper library including a multi-request implementation.
 *
 * Copyright (C) 2011 Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License 2.1 as published by the Free Software Foundation; see
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library in file CurlyLicense.txt; if not, write
 * to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA or see at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 */

/**
 * Basic request handling interface
 */
interface CurlyRequestHandler {
	/**
	 * Called by Curly::multiRequest when a request has completed.
	 *
	 * @param $data The request response data
	 * @param $request The completed cURL request handle
	 * @return true, if multiRequest should continue processing requests, false if it should stop.
	 */
	public function processResult ($data, $request);
}

/**
 * Curly is a simple cURL wrapper library.
 */
abstract class Curly {

	/** Default connection limit. */
	const MAXCONNECTIONS = 5;
	
	/** Default options used for any request. Additionally, Curly forces CURLOPT_RETURNTRANSFER = true. */
	private static $defaultOptions = array (
	   CURLOPT_HEADER         => false
	  ,CURLOPT_SSL_VERIFYPEER => false
	  ,CURLOPT_TIMEOUT        => 20   // seconds.
	  ,CURLOPT_FOLLOWLOCATION => true // Handle HTTP redirects (301 - Moved permanently, 302 - moved temporarily)
	  ,CURLOPT_MAXREDIRS      => 5    // Limit redirect chains
	);
	
	/**
	 * file_get_contents replacement using CURL.
	 * 
	 * @param $url to read from
	 * @param $userAgent (optional) user-agent header string to use
	 * 
	 * @return string Server response (data only, without headers); null if any error occurred.
	 */
	public static function getContents ($url, $userAgent = null) {
		$options = null;
		if (!$userAgent) $userAgent = ini_get ('user_agent');
		if ($userAgent) $options = array (CURLOPT_USERAGENT => $userAgent);		
		$data = self::singleRequest (self::newRequest ($url, null, $options), $errorMsg);
		if ($errorMsg === null) return $data;
		return null;	
	}
	
	/**
	 * Get the error status after a CURL request.
	 * @param $request cURL request handle
	 * @param $sizeOfResponse int
	 * @return string containing the error message, or null if no error
	 */
	public static function getError ($request, $sizeOfResponse) {
		// Get error/success info
		$errorMsg = null;
	    $info = curl_getinfo($request);
		if (curl_errno($request) != 0) {
	      $errorMsg = 'CURL error: ' . curl_error($request);
	    } else {
			if ($info['http_code'] != 200) {
      			$errorMsg = 'Bad response from server (' . $info['http_code'] . ')';
		    } else if ($sizeOfResponse == 0) {
		    	$errorMsg = 'Empty server response';
		    }
	    }
		// if ($errorMsg !== null) $errorMsg .= ' for ' . $info['url'];
		return $errorMsg;	    
	}
	
	/**
	 * Create a new cURL request with the specified options.
	 *
	 * @param $url string URL of the request
	 * @param $postData array if null, create a GET request, otherwise a POST request with the specified POST data
	 * @param $options array additional options.
	 * @return newly created cURL request handle
	 */
	public static function newRequest ($url, $postData, $options) {
		$request = curl_init();
		curl_setopt_array ($request, (is_array ($options)) ? ($options + self::$defaultOptions) : self::$defaultOptions);
		curl_setopt ($request, CURLOPT_URL, $url);
		curl_setopt ($request, CURLOPT_RETURNTRANSFER, true);
		if ($postData) {
			curl_setopt ($request, CURLOPT_POST, true);
			curl_setopt ($request, CURLOPT_POSTFIELDS, $postData);
		}
		return $request;
	}
	
	/**
	 * Create a new cURL POST request with the specified POST data and options.
	 *
	 * @param $url string URL of the request
	 * @param $postData array POST data
	 * @param $options array (optional) additional request options.
	 * @return newly created cURL request handle
	 */
	public static function postRequest ($url, $postData, $options = null) {
		return self::newRequest ($url, $postData ? $postData : array(), $options);
	}
	
	/**
	 * Create a new cURL GET request with the specified options.
	 *
	 * @param $url string URL of the request
	 * @param $options array (optional) additional request options.
	 * @return newly created cURL request handle
	 */
	public static function getRequest ($url, $options = null) {
		return self::newRequest ($url, null, $options);
	}
	
	/**
	 * Run a cURL request. The request is closed.
	 *
	 * @param $request cURL request handle
	 * @param $errorMsg &string returns an error message, if there was any error, or null upon success
	 * @return Data response from the executed request.
	 */
	public static function singleRequest ($request, &$errorMsg) {
		$data = curl_exec ($request);
		$errorMsg = self::getError($request, sizeof($data));
		curl_close ($request);
		return $data;
	}
		
	/**
	 * Run several cURL requests concurrently. When this function returns, all requests are closed.
	 *
	 * @param array $requests cURL request handles
	 * @param CurlyRequestHandler $requestHandler to handle completed requests
	 * @param int maxConnections (optional) maximum number of connections to use. Defaults to Curly::MAXCONNECTIONS.
	 * @return status code from {@link curl_multi_exec()}
	 */
	public static function multiRequest ($requests, $requestHandler, $maxConnections = Curly::MAXCONNECTIONS) {
		if (count ($requests) === 1) {
			// Single request.
			$requestHandler->processRequest (curl_exec ($requests[0]), $requests[0]);
			curl_close ($requests[0]);
			return CURLM_OK;
		}
		// Use curl_multi routines to spawn $requests in parallel. Stop the whole shebang when
		// the $requestHandler returns false.
		// We know that all are GET requests, with the same curl options.
		// Make at most five parallel requests to avoid overloading the server.
		if (!$maxConnections) $maxConnections = Curly::MAXCONNECTIONS;
		if (count ($requests) < $maxConnections) $maxConnections = count($requests);
		$multi = curl_multi_init();
		for ($last = 0; $last < $maxConnections; $last++) {
		  curl_multi_add_handle ($multi, $requests[$last]);			
		}
		// Now run the requests. Whenever one finishes, call the completed function. If that returns false,
		// terminate the whole shebang; otherwise, add a new request (if there are any left).
		$keepOn = true;
		do {
			$nofRequests = $last;
			do {
				$code = curl_multi_exec ($multi, $stillRunning);
			} while ($code === CURLM_CALL_MULTI_PERFORM);
			if ($code != CURLM_OK) break;
			while ($keepOn && ($info = curl_multi_info_read($multi))) { // Assignment in condition on purpose!
				// We have a completed request.
				$completed = $info['handle'];
				$data = curl_multi_getcontent ($completed);
				$keepOn = $requestHandler->processResult ($data, $completed);
				// One request done: add a new one, if we still got one
				if ($keepOn && $last < count ($requests)) {
				  curl_multi_add_handle ($multi, $requests[$last++]);
				}
				// Remove the completed request
				curl_multi_remove_handle ($multi, $completed);
			}
		  if (!$keepOn) break;
		  // Wait until something changes. If we added some, do not use select, but call curl_multi_exec
		  // above immediately to get the new requests going now.
		  if ($stillRunning && $last == $nofRequests) curl_multi_select ($multi);
		} while ($stillRunning  || $last > $nofRequests);
		curl_multi_close ($multi);
		// Close the remaining requests
		for (;$last < count($requests); $last++) {
			curl_close ($requests[$last]);
		}
		// Returns CURLM_OK even if the $requestHandler stopped processing. The handler is supposed to record failure
		// conditions, either from curl_getinfo of a completed connection or from data received, by other means
		// such that it can be checked after this function here has returned. We'll return an error code only
		// if curl_multi_exec indicates an error.
		return $code;
	}
}