Description,ImageDescription,Comment,XPComment,Caption-Abstract = #U\.?S\.?\s+Army\s+[Pp]hoto# > PD-USGov-Military-Army
CopyrightNotice = #[Pp]ublic\s+[Dd]omain# > PD-USGov-Military
Credit,OriginalTransmissionReference = #U.S. Department(\s+of\s+Defense)?# > PD-USGov-Military
#29456680@N06 = ISAF Public Affairs, http://www.flickr.com/people/isafmedia/