Description,ImageDescription,Caption-Abstract = #(U\.?S\.?\s+)?Marine\s+[Cc]orps\s+[Pp]hoto# > PD-USGov-Military-Marines
Description,ImageDescription,Caption-Abstract = #(U\.?S\.?\s+)?Air\s+[Ff]orce\s+[Pp]hoto# > PD-USGov-Military-Air Force
Description,ImageDescription,Caption-Abstract = #(U\.?S\.?\s+)?Army\s+[Pp]hoto# > PD-USGov-Military-Army
Description,ImageDescription,Caption-Abstract = #(U\.?S\.?\s+)?Navy\s+[Pp]hoto# > PD-USGov-Military-Navy
Description,ImageDescription,Caption-Abstract = #D[Oo]D\s+[Pp]hoto\s+by# > PD-USGov-Military
Copyright,CopyrightNotice,Rights = #Public\s+[Dd]omain# > PD-USGov-Military
OriginalTransmissionReference = #^AFCENT$# > PD-USGov-Military-Air Force
#U.S. Central Command (CENTCOM)