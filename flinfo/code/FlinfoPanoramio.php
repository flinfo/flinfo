<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('FlinfoStatus.php');
require_once ('FlinfoIn.php');

require_once ('lib/Curly.php');

/**
 * Input handler for panoramio. Uses screenscraping.
 */
class FlinfoPanoramio extends FlinfoIn {

	private $mRawId = null;
	private $mStatus = null;
	private $mId = null;
	private $mSizes = null;
	private $mUserId = null;
	private $mUserName = null;
	private $mUserUrl = null;
	private $mTitle = null;
	private $mLicense = null;
	private $mRawLicense = null;
	private $mLicenseUrl = null;
	private $mGeo = null;
	private $mDate = 0;
	private $mDescUrl = null;
	private $mTags = null;
	private $mPlace = null;

	public function __construct ($parameterFileName, $requestParams) {
		// Nothing to do.
	}

	private function extractId ($rawId) {
	    if (preg_match ('/^\d+$/', $rawId)) {
			// All digits: assume a photo id.
			return $rawId;
	    } else if (preg_match ('!^https?://(commondatastorage\.googleapis\.com/)?static\.panoramio\.com/photos/original/(\d+)\.jpg$!', $rawId, $matches)) {
	    	return $matches[2];
	    } else if (preg_match ('!^https?://([^/.]+\.)?panoramio\.com/photo/(\d+)!', $rawId, $matches)) {
	    	return $matches[2];
	    } else if (preg_match ('!^https?://([^/]+\.)?google.com/[^/]*panoramio/photos/[^/]+/(\d+).jpg$!', $rawId, $matches)) {
	    	return $matches[2];
		} else {
			// Id could not be determined
			return null;
		}
	}

	public function getInfo ($id) {
		$this->mRawId = $id;
		$this->mId = $this->extractId ($id);
		if (!$this->mId) {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
			return array ($id, $this->mStatus);
		}
		$this->mDescUrl = "http://www.panoramio.com/photo/" . $this->mId;
		$info = Curly::getContents ($this->mDescUrl);
		if (!$info) {
			$this->mStatus = FlinfoStatus::STATUS_SERVER_FAILURE;
			return array ($this->mId, $this->mStatus);
		}
		// Sanity check
		if (!preg_match ('/\b(photo_id|photoId)\s=\s' . preg_quote ($this->mId, '/') . '[\s[,;]/', $info)) {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
			return array ($this->mId, $this->mStatus);
		}
		// Extract the info from the HTML (Screenscraping)
		if (preg_match ('!<p\s+id="author">(\s|\S)*?<a\s+href="/user/([^?"]*)[^"]*">([^<>]*)</a>!', $info, $matches)) {
			$this->mUserId = $matches[2];
			$this->mUserName = trim ($matches[3]);
			$this->mUserUrl = "http://www.panoramio.com/user/" . $this->mUserId;
		} else if (preg_match('!<a\s+href="/user/([^?"]*)[^"]*"\s+rel="author">([^<>]*)</a>!', $info, $matches)) {
			$this->mUserId = $matches[1];
			$this->mUserName = trim ($matches[2]);
			$this->mUserUrl = "http://www.panoramio.com/user/" . $this->mUserId;
		}
		if (preg_match ('!<h1\s+id="photo-title"[^>]*>([^<>]*?)</h1>!', $info, $matches)) {
			$this->mTitle = trim ($matches[1]);
		}
		if (preg_match ('/<a\s+id="main-photo"\s+href=\s*"([^"]*)"\s+title=\s*"([^"]*)">\s*<img\s+src=\s*"([^"]*)"(\s|\S)+?width=\s*"(\d+)"\s+height=\s*"(\d+)"/', $info, $matches)) {
			$this->mSizes = array ();
			$orgUrl = $matches[1];
			if (!preg_match('!^https?://!', $orgUrl)) {
				$orgUrl = 'http://www.panoramio.com' . $orgUrl;
			}
			$orgTitle = $matches[2];
			$orgWidth = 0;
			$orgHeight = 0;
			$imgUrl = $matches[3];
			$this->mSizes[] = array ("width" => 60, "height" => 60, "source" => str_replace ("/medium/", "/square/", $imgUrl));
			// Don't know the size of this one, so let it be
			// $this->mSizes[] = array ("width" => 0, "height" => 0, "source => str_replace ("/medium/", "/small/", $matches[3]));
			$this->mSizes[] = array ("width" => $matches[5] + 0, "height" => $matches[6] + 0, "source" => $imgUrl);
			if (preg_match ('/\s(\d+)\s*x\s*(\d+)\s+pixels/', $orgTitle, $matches)) {
				$orgWidth = $matches[1] + 0;
				$orgHeight = $matches[2] + 0;
			}
			// $orgUrl isn't usable anymore as of March 2013. Construct a static url instead.
			if (preg_match('!/([^/]+)$!', $imgUrl, $matches)) {
				$orgUrl = 'http://static.panoramio.com/photos/original/' . $matches[1];
			}
			$this->mSizes[] = array ("width" => $orgWidth, "height" => $orgHeight, "source" => $orgUrl);
		} else if (preg_match ('/<img\s+src=\s*"([^"]*)"\s+[^>]*id="main-photo_photo"[^>]*>/', $info, $matches)) {
			// Another panoramio design...
			$this->mSizes = array ();
			$imgUrl = $matches[1];
			if (preg_match('!/([^/]+)$!', $imgUrl, $matches)) {
				$imgFileName = $matches[1];
				$this->mSizes[] = array ("width" => 60, "height" => 60, "source" => 'http://static.panoramio.com/photos/square/' . $imgFileName);
				$this->mSizes[] = array ("width" => -1, "height" => -1, "source" => 'http://static.panoramio.com/photos/original/' . $imgFileName);
			} else {
				$this->mSizes[] = array ("width" => -1, "height" => -1, "source" => $imgUrl);
			}
		}
		if (preg_match ('/<li\s+class="license ([^"]*)"(\s*>\s*<a\s+href="([^"]*)")?/', $info, $matches)) {
			$this->mRawLicense = $matches[1];
			if (count ($matches) > 3 && $matches[3] !== null && $matches[3] != "") $this->mLicenseUrl = $matches[3];
			switch ($this->mRawLicense) {
				case "by" :
					$this->mLicense = "cc-by";
					break;
				case "by-sa" :
					$this->mLicense = "cc-by-sa";
					break;
				default:
					$this->mLicense = null;
					break;
			}
			if ($this->mLicense !== null && $this->mLicenseUrl !== null) {
				$tag = self::ccLicenseFromUrl ($this->mLicenseUrl);
				if ($tag) $this->mLicense = $tag;
			}
		}
		$lat = null;
		if (preg_match ('/<abbr\s+class="latitude"\s+title="([^"]+)"\s*>/', $info, $matches)) {
			$lat = $matches[1];
		}
		$lon = null;
		if (preg_match ('/<abbr\s+class="longitude"\s+title="([^"]+)"\s*>/', $info, $matches)) {
			$lon = $matches[1];
		}
		if ($lat !== null && $lon !== null) {
			$this->mGeo = array ("latitude" => $lat, "longitude" => $lon, "source" => "panoramio");
		}
		if (preg_match ('!<li\s+id="tech-details"(\s|\S)*?<ul>(\s|\S)*?<li>(\s|\S)*?((\d{4})/(\d\d)/(\d\d) (\d\d):(\d\d):(\d\d))</li>!', $info, $matches)) {
			$this->mRawDate = $matches[4];
			$this->mDate = mktime($matches[8], $matches[9], $matches[10], $matches[6], $matches[7], $matches[5]);
		}
		// Try to find all tags
		$offset = 0;
		while (preg_match ('!<li\s+id="tag_element_\d+"\s*>\s*<a[^>]*>\s*([^<]*)</a\s*>\s*</li\s*>!', $info, $matches, PREG_OFFSET_CAPTURE, $offset)) {
			$offset = $matches[0][1] + strlen ($matches[0][0]);
			$tag = trim ($matches[1][0]);
			if ($tag !== null && $tag != "") {
				if ($this->mTags === null) $this->mTags = array ();
				$this->mTags[] = $tag;
			}
		}
		// Try to get the place name(s)
		if (preg_match ('!<p\s+id="place"\s*>\s*in\s+([^<]*)</p\s*>!', $info, $matches)) {
			$places = trim ($matches[1]);
			if ($places !== null && $places != "") {
				$places = preg_split ('/\s*,\s*/', $places);
				foreach ($places as $place) {
					if ($place !== null && $place != "") {
						if ($this->mPlace === null) $this->mPlace = array ();
						$this->mPlace[] = $place;
					}
				}
			}
		}
		$this->mStatus = FlinfoStatus::STATUS_OK;
		return array ($this->mId, $this->mStatus);
	}

	public function getAccountId () {
		return $this->mUserId;
	}

	public function getAuthor () {
		return array (array ($this->mUserUrl, $this->mUserName, null));
	}

	public function getSource () {
		return array ($this->mDescUrl, $this->mTitle ? $this->mTitle : 'panoramio');
	}

	private function getLicenseName ($license) {
		if ($license == "c") return "All rights reserved";
		return $license;
	}

	public function getLicenses ($goodUser) {
		$tags = array ();
		$status = $this->getLicenseName ($this->mRawLicense);
		if ($goodUser) {
			if ($this->mLicense) {
				$tags[] = $this->mLicense;
				$status = null;
				$tags[] = $this->getReviewTag();
			}
		}
		return array ($status, $tags, null);
	}

	protected function internalGetReviewTag () {
		return "panoramioreview";
	}

	public function getTitle () {
		return $this->mTitle;
	}

	public function getDescription () {
		return ""; // Panoramio has no defined image description
	}

	public function getCategories () {
		$tags = array ();
		if ($this->mTags) $tags = array_merge ($tags, $this->mTags);
		if ($this->mPlace) $tags = array_merge ($tags, $this->mPlace);
		return $tags;
	}

	public function getDate () {
		return $this->mDate;
	}

	public function getGeoInfo () {
		return $this->mGeo;
	}

	public function getSizes () {
		return $this->mSizes;
	}

	/**
	 * Fake a JSON server result and return that.
	 *
	 * @return Faked "raw" server result.
	 */
	public function getRawResult () {
		if ($this->mStatus === null) return null;
		$result = array ("status" => $this->mStatus, "raw_id" => $this->mRawId);
		$photo = null;
		if ($this->mId !== null) {
			$photo = array ();
			$photo["id"] = $this->mId;
		}
		if ($this->mStatus == FlinfoStatus::STATUS_OK) {
			$photo["url"] = $this->mDescUrl;
			$photo["title"] = $this->mTitle;
			$photo["owner"] = array ("userId" => $this->mUserId, "userName" => $this->mUserName, "userPage" => $this->mUserUrl);
			$photo["license"] = array ("key" => $this->mRawLicense, "link" => $this->mLicenseUrl);
			if ($this->mDate) $photo["date"] = $this->mRawDate;
			$photo["sizes"] = $this->mSizes;
			if ($this->mGeo) $photo["geo"] = $this->mGeo;
			if ($this->mPlace) $photo["place"] = $this->mPlace;
			if ($this->mTags) $photo["tags"] = $this->mTags;
		}
		if ($photo) $result["photo"] = $photo;
		return $result;
	}

}